# StoreMessage
For this project I created a really simple Smart Contract in Solidity. It has a function for storing a string and retrieving the input value. I added a front-end to this Smart Contract to make it a DApp. I created this front-end in ReactJS, a JavaScript library for building user interfaces. Last but not least I created a Dockerfile so I can easily move the project from one server to one other.

## Try it out
You can test this StoreMessage Smart Contract DApp on https://basabbink-storemessage.netlify.app/. This project runs on the Kovan Testnetwork of Ethereum, if you want to give it a go and try it you need to have MetaMask installed and run it on the Kovan Network. You also need some test Ether, you can get this from a Kovan Testnetwork Faucet like: https://faucet.kovan.network/.

When you click on the *Retrieve String* button you will recieve the latest string that has been inputted in the Smart Contract. You can put a message in the Smart Contract by typing in a message in the input field and clicking on "Submit to Blockchain".

## TODO
- Make project DRY proof 
  - App.js
    - const web3 = await Moralis.Web3.enable();
    - const contract = new web3.eth.Contract(abi, smartContractAddress);
