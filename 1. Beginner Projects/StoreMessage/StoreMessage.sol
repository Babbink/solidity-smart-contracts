// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

// Start by creating a Smart Contract backed DApp which takes an input string for a greeting, stores to the state and prints to a label on the page.
// This will show that you can create a full-stack Blockchain application by connecting a basic back-end to a simple front end.

contract StoreMessage {
    
    string input;
     
    function storeString(string memory _input) public {
        input = _input;
    }
    
    function retrieve() public view returns (string memory){
        return input;
    }
    
}
