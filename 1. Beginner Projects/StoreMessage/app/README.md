# HelloWorld DApp
This DApp stores on and recieves a message from the blockchain.

## How to run
### Inside Docker
1. Install docker
2. ```docker run -d -it -p 80:80 basabbink/helloworld```
3. Go to http://localhost:80/

### From local machine
1. ```npm install```
2. ```npm start```

## How to recreate
1. Clone this project.
2. Open a terminal.
3. Use ```cd``` to go to the correct folder (solidity-smart-contracts\1. Beginner Projects\HelloWorld\app).
4. Type ```npm install``` or ```yarn install``` to install the needed packages.
5. Go to https://remix.ethereum.org and copy and paste everything in *HelloWorld.sol* to a new file.
6. Press ```ctrl + s``` to save and compile the file.
7. Click on the 2nd option on the left hand side of the window (Solidity Compiler)
8. Click on the blue *Compile HelloWorld.sol* button.
9. On the bottom you see ABI with a copy icon, click on that and paste it into the *HelloWorld-abi.json*.
10. Click on the 3rd option on the left hand side of the window (Deploy & Run Transactions).
11. To deploy the Smart Contract on the a Ethereum test network you need to have a web3 compatible wallet (like MetaMask) and some test Ether. If you have this, click on *JavaScript VM* and change it to *Injected Web3*.
12. Click on the orange *Deploy* button. A MetaMask popup will appear, accept this to publish it on the blockchain. Make sure you are on a test netwerk if you only want to test this.
13. A Smart Contract is deployed. You will see a contract like *HELLOWORLD AT ...* on the bottom left hand side with a copy icon, click on it. Paste this within *App.js* after ```const smartContractAddress = ""``` 
14. Type ```npm start``` to test the project.