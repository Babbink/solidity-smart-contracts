import React, { useState } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import abi from "./StoreMessage-abi.json";

const Moralis = require("moralis");
const smartContractAddress = "0x9F1D375fa30b6f577B579b097328982C61E84080";

function App() {
  const [string, setString] = useState(" ");
  const [storedString, setStoredString] = useState(" ");

  const storeString = async (e) => {
    if (e) e.preventDefault();

    try {
      const web3 = await Moralis.Web3.enable();
      const contract = new web3.eth.Contract(abi, smartContractAddress);
      const accounts = await web3.eth.getAccounts();

      await contract.methods.storeString(string).send({ from: accounts[0] });
    } catch (err) {
      console.log(err.message);
    }
  };

  const recieveString = async (e) => {
    if (e) e.preventDefault();

    try {
      const web3 = await Moralis.Web3.enable();
      const contract = new web3.eth.Contract(abi, smartContractAddress);
      setStoredString(await contract.methods.retrieve().call());
    } catch (err) {
      console.log(err.message);
    }
  };

  setInterval(recieveString, 1000);

  return (
    <Container className="text-center">
      <Row className="mx-auto my-5">
        <Col>
          <h1>StoreMessage DApp</h1>
          <h2>Created by Bas Abbink</h2>
        </Col>
      </Row>
      <Row className="mx-auto my-5">
        <Col>
          <Form>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>What message do you want to send to the blockchain?</Form.Label>
              <Form.Control type="text" placeholder="Message" style={{ display: "inline-block", "text-align": "center" }} onChange={(e) => setString(e.target.value)} />
              <Form.Text className="text-muted">Everyone can see all messages send to this Smart Contract at any time</Form.Text>
            </Form.Group>

            <Button variant="primary" type="submit" onClick={(e) => storeString(e)}>
              Submit
            </Button>
          </Form>
        </Col>
        <Col>
          <p>
            <b>Stored message:</b>
          </p>
          <p>{storedString}</p>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
