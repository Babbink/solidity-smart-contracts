# Calculator
For this project I created a really simple Smart Contract in Solidity. This contract contains multiple calculation functions: plus, minus, times and divide functionality. I added a front-end to this Smart Contract to make it a DApp. I created this front-end in ReactJS, a JavaScript library for building user interfaces. Last but not least I created a Dockerfile so I can easily move the project from one server to one other.

## Try it out
You can test this Calculator Smart Contract DApp on https://basabbink-calculator.netlify.app/. This project runs on the Kovan Testnetwork of Ethereum.

Fill in the first value (number), a calculation type and a second value (number). Once you have done that you can click on *Calculate*, your answer will than be shown on the page.

## TODO
- Remove yellow warnings from https://remix.ethereum.org
