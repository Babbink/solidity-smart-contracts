// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

// A calculator with plus, minus, times and divide functionality.
// Take two integer inputs and a drop-down box for operation, pass to a Smart Contract which calculates the result and display on the page.

contract Calculator {
    
    function plus(uint value, uint plus) public pure returns (uint) {
        return (value + plus);
    }
    
    function minus(uint value, uint minus) public pure returns (uint) {
        return (value - minus);
    }
    
    function times(uint value, uint times) public  pure returns (uint) {
        return (value * times);
    }
    
    function divide(uint value, uint devidedBy) public pure returns (uint) {
        return (value / devidedBy);
    }
}
