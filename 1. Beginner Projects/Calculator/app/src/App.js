import React, { useState } from "react";
import { Container, Row, Col, Button, Form, Dropdown } from "react-bootstrap";
import abi from "./Calculator-abi.json";

const Moralis = require("moralis");
const smartContractAddress = "0x3e71700B5D7Ea7abf0ab59cF6d2B213dc8F00dAB";

function App() {
  const [calculate, setCalculate] = useState({ typeValue: "Calculation type", answer: "" });

  const calculateOutcome = async (e) => {
    if (e) e.preventDefault();

    const web3 = await Moralis.Web3.enable();
    const contract = new web3.eth.Contract(abi, smartContractAddress);
    const accounts = await web3.eth.getAccounts();
    const { type, valueOne, valueTwo } = calculate;

    if (valueOne && valueTwo) {
      switch (type) {
        case "plus":
          setCalculate({ ...calculate, answer: await contract.methods.plus(valueOne, valueTwo).call({ from: accounts[0] }) });
          break;
        case "minus":
          setCalculate({ ...calculate, answer: await contract.methods.minus(valueOne, valueTwo).call({ from: accounts[0] }) });
          break;
        case "times":
          setCalculate({ ...calculate, answer: await contract.methods.times(valueOne, valueTwo).call({ from: accounts[0] }) });
          break;
        case "divide":
          setCalculate({ ...calculate, answer: await contract.methods.divide(valueOne, valueTwo).call({ from: accounts[0] }) });
          break;
        default:
          setCalculate({ ...calculate, answer: "Choose a calculation type" });
          break;
      }
    } else {
      setCalculate({ ...calculate, answer: "Fill in all the fields" });
    }
  };

  return (
    <Container className="text-center">
      <Row className="mx-auto my-5">
        <Col>
          <h1>Calculator DApp</h1>
          <h2>Created by Bas Abbink</h2>
        </Col>
      </Row>
      <Row className="mx-auto my-5">
        <Col>
          <Form.Control type="number" placeholder="Value 1" onChange={(e) => setCalculate({ ...calculate, valueOne: e.target.value })} />
        </Col>
        <Col>
          <Dropdown>
            <Dropdown.Toggle variant="dark" id="dropdown-basic">
              {calculate.typeValue}
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item onClick={() => setCalculate({ ...calculate, type: "plus", typeValue: "+" })}>+</Dropdown.Item>
              <Dropdown.Item onClick={() => setCalculate({ ...calculate, type: "minus", typeValue: "-" })}>-</Dropdown.Item>
              <Dropdown.Item onClick={() => setCalculate({ ...calculate, type: "times", typeValue: "x" })}>x</Dropdown.Item>
              <Dropdown.Item onClick={() => setCalculate({ ...calculate, type: "divide", typeValue: "/" })}>/</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Col>
        <Col>
          <Form.Control type="number" placeholder="Value 2" onChange={(e) => setCalculate({ ...calculate, valueTwo: e.target.value })} />
        </Col>
        <Col>
          <Button variant="dark" onClick={calculateOutcome}>
            Calculate
          </Button>
        </Col>
      </Row>
      <Row className="mx-auto my-5">
        <Col>
          <h1>{calculate.answer}</h1>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
