# HelloWorld DApp
This DApp stores on and recieves a message from the blockchain.

## How to run
### Inside Docker
1. Install docker
2. ```docker run -d -it -p 80:80 basabbink/helloworld```
3. Go to http://localhost:80/

### From local machine
1. ```npm install```
2. ```npm start```