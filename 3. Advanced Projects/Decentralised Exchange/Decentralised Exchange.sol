// Create an on-chain exchange with the ability to exchange Ether for ERC20 tokens and vice-versa. EtherDelta is an example of an on-chain exchange. Make it so anyone can add the address of any ERC20 token so the list can grow.

// For extra points, use an off-chain order filling algorithm to quickly fill market orders.