// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

// Add VotingFactory to let users create votings and proposals

contract Voting {
    
    address public manager;
    uint public phase = 1;
   
    struct Voter {
        bool voted;
        address voter;
        uint vote;
    }
    mapping(address => Voter) public voters;

    struct Proposal {
        string name;
        uint voteCount;
    }
    Proposal[] public proposals;

    constructor(string[] memory proposalNames) {
        manager = msg.sender;

        for (uint i = 0; i < proposalNames.length; i++) {
            proposals.push(Proposal({name: proposalNames[i], voteCount: 0}));
        }
    }
    
    function register() public {
        require(phase == 1, "Not is this phase (yet)");
        
        Voter storage newVoter = voters[msg.sender];
        
        newVoter.voter = msg.sender;
        newVoter.voted = false;
        newVoter.vote = 0;
    }
    
    function vote(uint proposal) public {
        Voter storage voter = voters[msg.sender];
        require(phase == 2, "Not is this phase (yet)");
        require(!voter.voted, "You have already voted!");
        
        voter.voted = true;
        voter.vote = proposal;

        proposals[proposal].voteCount += 1;
    }
    
    function winningProposal() public view returns (uint winningProposal_) {
        uint winningVoteCount = 0;
        for (uint p = 0; p < proposals.length; p++) {
            if (proposals[p].voteCount > winningVoteCount) {
                winningVoteCount = proposals[p].voteCount;
                winningProposal_ = p;
            }
        }
    }
    
    function winnerName() public view returns (string memory winnerName_) {
        winnerName_ = proposals[winningProposal()].name;
    }
    
    function nextPhase() public {
        require(msg.sender == manager, "You are not the manager, you can't execute this function");
        
        if (phase < 3) {
            phase = phase + 1;
        } else {
            phase = 1;
        }
    }
}
