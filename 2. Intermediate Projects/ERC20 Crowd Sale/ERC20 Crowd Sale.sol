// Write a DApp which allows users to purchase your ERC20 token from the browser using Eth.

// Set a limited time frame for the crowd sale.

// Add an early investor bonus to entice users to buy in early. “First x number of investors get x percentage free tokens”.

// Library Examples

// Intermediate developers should get used to using third-party libraries to extend useful functionality. Openzeppelin provides useful Smart Contracts which aid development. For the voting DApp, the Ownable contract would be useful to restrict access to certain functions. ERC20 is a well-known standard and is implemented by Openzeppelin.