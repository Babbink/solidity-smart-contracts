// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract TodoListFactory {
    
    struct todoList {
        address todoListAddress;
    }
    
    mapping(address => todoList) todoLists;
    uint todoListsCounter = 0;

    function createTodoList() public {
        address todoListAddress = address(new TodoList(msg.sender));
        todoLists[msg.sender] = todoList(todoListAddress);
        todoListsCounter++;
    }
    
    function getTodoList(address owner) public view returns(todoList memory) {
        return todoLists[owner];
    }
    
    function countTodoLists() public view returns(uint) {
        return todoListsCounter;
    }
}

contract TodoList {
    
    address manager;
    
    struct Item {
        string title;
        string description;
        uint priority;
        bool isFinished;
    }
    mapping (address => Item) items;
    Item[] itemList;
    
    constructor(address _manager) {
        manager = _manager;    
    }
    
    function addItem(string memory _title, string memory _description, uint _priority) public {
        require (manager == msg.sender, "You are not the owner of this TodoList");
        Item storage item = items[msg.sender];
        
        item.title = _title;
        item.description = _description;
        item.priority = _priority;
        item.isFinished = false;
        
        itemList.push(item);
    }
    
    function updateTitle(uint itemID, string memory _newTitle) public {
        require (manager == msg.sender, "You are not the owner of this TodoList");
        itemList[itemID].title = _newTitle;
    }
    
    function updateDescription(uint itemID, string memory _newDescription) public {
        require (manager == msg.sender, "You are not the owner of this TodoList");
        itemList[itemID].description = _newDescription;
    }
    
    function updatePriority(uint itemID, uint _newPriority) public {
        require (manager == msg.sender, "You are not the owner of this TodoList");
        if (itemList[itemID].priority != _newPriority) itemList[itemID].priority = _newPriority;
    }
    
    function updateItemStatus(uint itemID, bool _newStatus) public {
        require (manager == msg.sender, "You are not the owner of this TodoList");
        if (itemList[itemID].isFinished != _newStatus) itemList[itemID].isFinished = _newStatus;
    }
    
    function deleteItem(uint itemID) public {
        require (manager == msg.sender, "You are not the owner of this TodoList");
        itemList[itemID] = itemList[itemList.length - 1];
        itemList.pop();
    }
    
    function getAllItems() public view returns(Item[] memory) {
        require (manager == msg.sender);
        return itemList;
    }
    
    function getItem(uint index) public view returns(Item memory) {
        require (manager == msg.sender);
        return itemList[index];
    }
}