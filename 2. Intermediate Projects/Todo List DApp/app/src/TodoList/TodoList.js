import React, { useState, useEffect } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

import todoListABI from "./TodoListDApp-todolist-abi.json";

import AddItem from "./TodoItems/AddItem";
import RenderOnlyTodoItems from "./RenderOnlyTodoItems";

const Moralis = require("moralis");

function TodoList(props) {
  const { todoListAddress } = props.match.params;
  const [items, setItems] = useState("");

  const getItems = async () => {
    const web3 = await Moralis.Web3.enable();
    const contract = new web3.eth.Contract(todoListABI, todoListAddress);
    const accounts = await web3.eth.getAccounts();

    setItems(await contract.methods.getAllItems().call({ from: accounts[0] }));
  };

  // Call only when loaded, and update when update is needed
  useEffect(() => {
    getItems();
  }, [items]);

  return (
    <Container className="text-center">
      <Row className="mx-auto my-5">
        <Col>
          <h1>ToDo List DApp</h1>
          <h2>Created by Bas Abbink</h2>
          <h3>{todoListAddress}</h3>
        </Col>
      </Row>
      {AddItem(todoListAddress)}
      {RenderOnlyTodoItems(todoListAddress, items)}
      {/* <Row className="mx-auto my-5">
        <Col>
          <Button variant="danger">Delete all items</Button>
        </Col>
        <Col>
          <Link to={`/${todoListAddress}/all`}>
            <Button variant="success">See all items</Button>
          </Link>
        </Col>
      </Row> */}
    </Container>
  );
}

export default TodoList;
