import React, { useState } from "react";
import { Row, Col, Form, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import todoListABI from "../TodoListDApp-todolist-abi.json";

const Moralis = require("moralis");

function AddItem(todoListAddress) {
  const [item, setItem] = useState({});

  const addItem = async () => {
    const web3 = await Moralis.Web3.enable();
    const contract = new web3.eth.Contract(todoListABI, todoListAddress);
    const accounts = await web3.eth.getAccounts();

    const { title, description, priority } = item;

    if (title && description && priority) {
      contract.methods
        .addItem(title, description, priority)
        .send({ from: accounts[0] })
        .then(() => setItem({ priority: "", title: "", description: "" }));
    }
  };

  return (
    <Row className="mx-auto my-5">
      <Col>
        <h1>Create new item</h1>

        <Form className="mx-auto my-3">
          <Form.Row>
            <Col xs={2}>
              <Form.Control placeholder="Priority" onChange={(e) => setItem({ ...item, priority: e.target.value })} value={item.priority} />
            </Col>
            <Col xs={3}>
              <Form.Control placeholder="Title" onChange={(e) => setItem({ ...item, title: e.target.value })} value={item.title} />
            </Col>
            <Col xs={5}>
              <Form.Control placeholder="Description" onChange={(e) => setItem({ ...item, description: e.target.value })} value={item.description} />
            </Col>
            <Col xs={2}>
              <Link to={`/${todoListAddress}`}>
                <Button onClick={addItem}>Add item</Button>
              </Link>
            </Col>
          </Form.Row>
        </Form>
      </Col>
    </Row>
  );
}

export default AddItem;
