import React, { useState, useEffect } from "react";
import { Container, Row, Col, Form, Button, Table } from "react-bootstrap";
import todoListABI from "../TodoListDApp-todolist-abi.json";

const Moralis = require("moralis");

function UpdateItem(props) {
  const { todoListAddress, index } = props.match.params;
  const [items, setItems] = useState("");
  const [item, setItem] = useState("");

  const updateItem = async () => {
    try {
      const web3 = await Moralis.Web3.enable();
      const contract = new web3.eth.Contract(todoListABI, todoListAddress);
      const accounts = await web3.eth.getAccounts();

      const { title, description, priority } = item;

      if (title && items[index].title !== title) contract.methods.updateTitle(index, title).send({ from: accounts[0] });

      if (description && items[index].description !== description) contract.methods.updateDescription(index, description).send({ from: accounts[0] });

      if (priority && items[index].priority !== priority) {
        console.log(typeof(priority));
        console.log(priority);
        if (typeof priority === "number") contract.methods.updatePriority(index, priority).send({ from: accounts[0] });
        else throw Error("You did not enter a number for the priority");
      }
    } catch (err) {
      alert(err.message);
    }
  };

  const getItems = async () => {
    const web3 = await Moralis.Web3.enable();
    const contract = new web3.eth.Contract(todoListABI, todoListAddress);

    setItems(await contract.methods.getAllItems().call());
  };

  useEffect(() => {
    getItems();
  }, []);

  console.log(items);

  const tableBody = () => {
    if (items[index]) {
      return (
        <tbody>
          <tr key={index}>
            <td>{items[index].priority}</td>
            <td>{items[index].title}</td>
            <td>{items[index].description}</td>
          </tr>
        </tbody>
      );
    }
    return;
  };

  return (
    <Container className="text-center">
      <Row className="mx-auto my-5">
        <Col>
          <h1>Update item</h1>
          <h3>
            Only fill in the <i>need to be updated</i> item components
          </h3>
          <p>To save some money on gas fees, only update the items you need to update</p>
        </Col>
      </Row>

      <Row>
        <Col>
          <Table bordered>
            <thead>
              <tr>
                <th>Priority</th>
                <th>Todo Item</th>
                <th>Description</th>
              </tr>
            </thead>
            {tableBody()}
          </Table>
        </Col>
      </Row>

      <Row>
        <Col>
          <Form className="mx-auto my-3">
            <Form.Row>
              <Col xs={2}>
                <Form.Control placeholder="New priority" onChange={(e) => setItem({ ...item, priority: parseInt(e.target.value) })} />
              </Col>
              <Col xs={3}>
                <Form.Control placeholder="New title" onChange={(e) => setItem({ ...item, title: e.target.value })} />
              </Col>
              <Col>
                <Form.Control placeholder="New description" onChange={(e) => setItem({ ...item, description: e.target.value })} />
              </Col>
            </Form.Row>
          </Form>

          <Button onClick={updateItem}>Update Item</Button>
        </Col>
      </Row>
    </Container>
  );
}

export default UpdateItem;
