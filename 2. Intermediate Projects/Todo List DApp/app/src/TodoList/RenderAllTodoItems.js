import React, { useState, useEffect } from "react";
import { Row, Col, Table, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import todoListABI from "./TodoListDApp-todolist-abi.json";

const Moralis = require("moralis");

function RenderAllTodoItems(todoListAddress) {
  const updateItemStatus = async (index, item) => {
    const web3 = await Moralis.Web3.enable();
    const contract = new web3.eth.Contract(todoListABI, todoListAddress);
    const accounts = await web3.eth.getAccounts();

    await contract.methods.updateItemStatus(index, !item.isFinished).send({ from: accounts[0] });
  };

  const deleteItem = async (index) => {
    const web3 = await Moralis.Web3.enable();
    const contract = new web3.eth.Contract(todoListABI, todoListAddress);
    const accounts = await web3.eth.getAccounts();

    await contract.methods.deleteItem(index).send({ from: accounts[0] });
  };

  const [items, setItems] = useState("");

  const getItems = async () => {
    const web3 = await Moralis.Web3.enable();
    const contract = new web3.eth.Contract(todoListABI, todoListAddress);

    setItems(await contract.methods.getAllItems().call());
    console.log("CALL ITEMS");
    return;
  };

  useEffect(() => {
    getItems();
  }, []);

  if (items) {
    return (
      <Row className="mx-auto my-5">
        <Col>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Priority</th>
                <th>Todo Item</th>
                <th>Description</th>
                <th>Edit</th>
                <th>Finish</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              {items.map((item, index) => {
                return (
                  <tr key={index}>
                    <td>{item.priority}</td>
                    <td>{item.title}</td>
                    <td>{item.description}</td>
                    <td>
                      <Link to={`/${todoListAddress}/${index}/edit`}>{<i className="fas fa-edit"></i>}</Link>
                    </td>
                    <td>
                      <Form.Check type="checkbox" id={`default-checkbox`} checked={item.isFinished} onChange={() => updateItemStatus(index, item)} />
                    </td>
                    <td>
                      <Link to={`/${todoListAddress}/`}>
                        <i className="fas fa-trash" onClick={() => deleteItem(index)}></i>
                      </Link>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </Col>
      </Row>
    );
  } else {
    return (
      <Row>
        <Col>
          <h1>Fetching items</h1>
        </Col>
      </Row>
    );
  }
}

export default RenderAllTodoItems;
