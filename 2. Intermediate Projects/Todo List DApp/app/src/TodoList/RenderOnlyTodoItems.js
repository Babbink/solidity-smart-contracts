import React from "react";
import { Row, Col, Table, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import todoListABI from "./TodoListDApp-todolist-abi.json";

const Moralis = require("moralis");

function RenderAllTodoItems(todoListAddress, items) {
  const updateItemStatus = async (index, item) => {
    const web3 = await Moralis.Web3.enable();
    const contract = new web3.eth.Contract(todoListABI, todoListAddress);
    const accounts = await web3.eth.getAccounts();

    await contract.methods.updateItemStatus(index, !item.isFinished).send({ from: accounts[0] });
  };

  const deleteItem = async (index) => {
    const web3 = await Moralis.Web3.enable();
    const contract = new web3.eth.Contract(todoListABI, todoListAddress);
    const accounts = await web3.eth.getAccounts();

    await contract.methods.deleteItem(index).send({ from: accounts[0] });
  };

  if (items.length <= 0) {
    return;
  } else if (items) {
    return (
      <Row className="mx-auto my-5">
        <Col>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th className="text-center">Priority</th>
                <th>Todo Item</th>
                <th>Description</th>
                <th className="text-center">Edit</th>
                <th className="text-center">Delete</th>
                <th className="text-center">Finish</th>
              </tr>
            </thead>
            <tbody>
              {items.map((item, index) => {
                if (item.isFinished === false) {
                  return (
                    <tr key={index}>
                      <td className="text-center">{item.priority}</td>
                      <td>{item.title}</td>
                      <td>{item.description}</td>
                      <td className="text-center">
                        <Link to={`/${todoListAddress}/${index}/edit`}>{<i className="fas fa-edit"></i>}</Link>
                      </td>
                      <td className="text-center">
                        <Link to={`/${todoListAddress}/`}>
                          <i className="fas fa-trash" onClick={() => deleteItem(index)}></i>
                        </Link>
                      </td>
                      <td className="text-center">
                        <Form.Check type="checkbox" id={`default-checkbox`} checked={item.isFinished} onChange={() => updateItemStatus(index, item)} />
                      </td>
                    </tr>
                  );
                }
                return null;
              })}
            </tbody>
          </Table>
        </Col>
      </Row>
    );
  } else {
    return (
      <Row>
        <Col>
          <h1>Fetching items</h1>
        </Col>
      </Row>
    );
  }
}

export default RenderAllTodoItems;
