import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";

import App from "./App";
import TodoList from "./TodoList/TodoList";
import UpdateItem from "./TodoList/TodoItems/UpdateItem";
import RenderAllTodoItems from "./TodoList/RenderAllTodoItems";

import "bootstrap/dist/css/bootstrap.min.css";

ReactDOM.render(
  <Router>
    <Route exact path="/" component={App}></Route>
    <Route exact path="/:todoListAddress" component={TodoList} />
    <Route exact path="/:todoListAddress/all" component={RenderAllTodoItems} />
    <Route exact path="/:todoListAddress/:index/edit" component={UpdateItem} />
  </Router>,
  document.getElementById("root")
);