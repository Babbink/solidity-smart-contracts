import React, { useState } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import factoryABI from "./TodoList/TodoListDApp-factory-abi.json";

const Moralis = require("moralis");
const smartContractAddress = "0x972A0aE041b2d9d6014f915D526F4c3856dd9b04";
const nonExistingAddress = "0x0000000000000000000000000000000000000000"

function App() {
  const [todoList, setTodoList] = useState(0);
  // DRY FIXEN
  // const web3 = await Moralis.Web3.enable();
  // const contract = new web3.eth.Contract(factoryABI, smartContractAddress);
  // const accounts = await web3.eth.getAccounts();

  // Create variable to reuse <Row .... <Col .... </Col></Row>
  // Maybe insert some data that will be shown, like button or text

  const createTodoList = async (e) => {
    if (e) e.preventDefault();

    const web3 = await Moralis.Web3.enable();
    const contract = new web3.eth.Contract(factoryABI, smartContractAddress);
    const accounts = await web3.eth.getAccounts();

    const hasContract = await contract.methods.getTodoList(accounts[0]).call();
    if (!hasContract.includes(nonExistingAddress)) {
      window.location.href = `/${hasContract}`;
      return;
    }

    contract.methods
      .createTodoList()
      .send({ from: accounts[0] })
      .then(() => {
        contract.methods
          .getTodoList(accounts[0])
          .call()
          .then((result) => {
            if (!result.includes(nonExistingAddress)) window.location.href = `/${result}`;
          });
      });
  };

  (async function getTodoList() {
    const web3 = await Moralis.Web3.enable();
    const contract = new web3.eth.Contract(factoryABI, smartContractAddress);
    const accounts = await web3.eth.getAccounts();

    contract.methods
      .countTodoLists()
      .call()
      .then((totalTodoListCount) => {
        setTodoList({ ...todoList, count: totalTodoListCount });
      });

    contract.methods
      .getTodoList(accounts[0])
      .call()
      .then((result) => {
        console.log(result);
        if (!result.includes("0x0000000000000000000000000000000000000000")) window.location.href = `/${result}`;
      });
  })();

  return (
    <Container className="text-center">
      <Row className="mx-auto my-5">
        <Col>
          <h1>Todo List DApp</h1>
          <h2>Created by Bas Abbink</h2>
        </Col>
      </Row>
      <Row className="mx-auto my-5">
        <Col>
          <h1>Already created {todoList.count} Todo Lists</h1>
        </Col>
      </Row>
      <Row className="mx-auto my-5">
        <Col>
          <Button onClick={createTodoList}>Create Todo List</Button>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
