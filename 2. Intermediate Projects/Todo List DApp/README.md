# Todo List
For this project I created an more advanced Smart Contract in Solidity with https://remix.ethereum.org. I created two contracts, the first one is creating the second one. I did this because you want to let your todo list be private and inmutable. The second reason was that it should be possible to create more than one todo list, so everyone can create a todo list on the blockchain. This is all in the first contract.

The second contract contains the functions for the todo list itself. You can add items, update their titles, description, priority and if they are finished or not. You can also, if you want to, delete an item. You cant restore this action after that.

I also added a front-end to this Smart Contract to make it a DApp. I created this front-end in ReactJS, a JavaScript library for building user interfaces. Last but not least I created a Dockerfile so I can easily move the project from one server to one other.

## Give it a try
You can create your own todo list with my Smart Contract DApp on *DOMAIN/IP OF THE SMART CONTRACT*. This project runs on the Kovan Testnetwork of Ethereum.

You can run this app on your local machine using Docker. Use the following command: `docker run --rm -d -p 3000:80 basabbink/todolist:test`, than you can go to `http://localhost/`.

## TODO
- Make it run on a live server (Raspberry Pi)
- App.js make it DRY
- Make Table Update the correct way
- AddItem, make button disabled until the transaction is confirmed and create a loading spinner during that time