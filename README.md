# Smart Contract
## By Bas Abbink (@Babbink)


## Try out my projects
### StoreMessage (Beginner Project)
https://basabbink-storemessage.netlify.app/

### Calculator (Beginner Project)
https://basabbink-calculator.netlify.app/

## Solidity
I got some of the ideas from the following websites:
- https://medium.com/blockcentric/ethereum-dapp-portfolio-ideas-21e1aac6dc52
- https://web3examples.com/ethereum/solidity_examples/
- https://docs.moralis.io/guides/build-a-simple-dapp-in-3-minutes
- https://cryptomarketpool.com/getting-started-with-solidity/

- https://medium.com/coinmonks/how-to-write-a-simple-smart-contract-25f6d3c1d6db
- https://medium.com/@bryn.bellomy/solidity-tutorial-building-a-simple-auction-contract-fcc918b0878a
- https://hackernoon.com/code-a-minimalistic-nft-smart-contract-in-solidity-on-ethereum-a-how-to-guide-kl3u34l6
- https://cryptomarketpool.com/how-to-create-an-nft-erc721-solidity-smart-contract/
- https://betterprogramming.pub/how-to-create-nfts-with-solidity-4fa1398eb70a
- https://programtheblockchain.com/posts/2018/01/30/writing-an-erc20-token-contract/
- https://www.blocksism.com/solidity-upgradable-token-contract/

## Chainlink
- https://docs.chain.link/docs/beginners-tutorial/