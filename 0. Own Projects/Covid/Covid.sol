pragma solidity >0.5.0;

contract Covid {
    struct Person  { 
      string name;
      address addr;
      
      uint vaccinatedCount;
      
      bool isTested;
      bool testResult;
      uint testedDate;
      
      uint temperature;
      uint temperatureDate;
   }
   
   mapping(address => Person) public persons;

   address public manager = msg.sender;

    // Can only be done by an organization
     function registerPerson(string memory name) public {
        persons[msg.sender] = Person(name, msg.sender, 0, false, false, 0, 0, 0);
    }
   
  
    // Can only be done by an organization 
    function registerVaccinated(address person) public {
        require(manager == msg.sender);
        persons[person].vaccinatedCount++;
    }
  
    // Can only be done by an organization
    function registerTested(address person, bool testResult) public {
        require(manager == msg.sender);
        persons[person].isTested = true;
        persons[person].testResult = testResult;
        persons[person].testedDate = block.timestamp;
    }

    // Can the person do by himself
    function registerTemperature(uint temperature) public {
        persons[msg.sender].temperature = temperature;
        persons[msg.sender].temperatureDate = block.timestamp;
    }
   
    function isAllowForFestival() public view returns(bool) {
        Person memory person = persons[msg.sender];
        if (person.temperature >= 35 && person.temperature <= 38 && person.temperatureDate + 86400 >= block.timestamp && (person.vaccinatedCount >= 2 || (person.isTested && !person.testResult && person.testedDate + 86400 >= block.timestamp))) return true;
        return false;   
    }
}
