// SPDX-License-Identifier: GPL-3.0
pragma solidity >0.4.17 <0.8;
pragma abicoder v2;

contract WebshopFactory {
    address[] deployedWebshops;
    
    function createWebshop(string memory name) public {
        address newWebshop = address(new Webshop(name, msg.sender));
        deployedWebshops.push(newWebshop);
    }
    
    function getDeployedWebshops() public view returns(address[] memory) {
        return deployedWebshops;
    }
}

contract Webshop {
    string shopName;
    address payable shopManager;
    
    constructor(string memory name, address payable creator) {
        shopName = name;
        shopManager = creator;
    }
    
    modifier restricted() {
        require(msg.sender == shopManager);
        _;
     }
    
    struct Product {
        string productName;
        uint weiPrice;
        uint stock;
        bool available;
    }

    Product[] products;

    function createProduct(string memory productName, uint price, uint stock, bool available) public restricted {
        available = available || false;
        
        // If stock equals 0 and the product is available, there can be sold an unlimited amount of that product
        // If a product has a certain stock, it will automatic ally become unavailable if there is no more stock
        products.push(Product({
            productName: productName,
            weiPrice: price,
            stock: stock,
            available: available
        }));
    }
    
    function deleteProduct(uint index) public restricted {
        delete products[index];
    }
    
    function updateProduct(uint index, string memory updatedProductName, uint updatedStock, uint updatedPrice, bool updatedAvailable) public restricted {
        products[index].productName = updatedProductName;
        products[index].stock = updatedStock;
        products[index].weiPrice = updatedPrice;
        products[index].available = updatedAvailable || products[index].available;
    }
    
    function updateProductName(uint index, string memory updatedProductName) public restricted {
        products[index].productName = updatedProductName;
    }
    
    function updateProductPrice(uint index, uint updatedPrice) public restricted {
        products[index].weiPrice = updatedPrice;
    }
    
    function updateProductStock(uint index, uint updatedStock) public restricted {
        products[index].stock = updatedStock;
    }
    
    function toggleAvailability(uint index) public restricted {
        products[index].available = !products[index].available;
    }
    
    function duplicateProduct(uint index) public restricted {
        products.push(Product({
            productName: products[index].productName,
            weiPrice: products[index].weiPrice,
            stock: products[index].stock,
            available: products[index].available
        }));
    }
    
    function buyProduct(uint index) public payable {
        require(products[index].available == true);
        require(msg.value == products[index].weiPrice);
        shopManager.transfer(msg.value);
        if (products[index].stock > 0) {
            products[index].stock = products[index].stock - 1;
            if (products[index].stock == 0) products[index].available = false;
        }
    }
    
    function getShopName() public view returns(string memory) {
        return shopName;
    }
    
    function getShopManager() public view returns(address) {
        return shopManager;
    }
    
    function getProducts() public view returns(Product[] memory) {
        return products;
    }
    
    function getProducts(uint index) public view returns(Product memory) {
        return products[index];
    }
}
