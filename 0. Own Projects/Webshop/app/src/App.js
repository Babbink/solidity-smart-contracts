import { Container, Row, Col, Button } from "react-bootstrap";
import factoryABI from "./WebshopFactory-abi.json";

const Moralis = require("moralis");
const smartContractAddress = "0xB1998D1FC4Acc2470a0A75e7f5C2094CB6Fcd967";

function App() {
  const createWebshop = async () => {
    const web3 = await Moralis.Web3.enable();
    const contract = new web3.eth.Contract(factoryABI, smartContractAddress);
    const accounts = await web3.eth.getAccounts();

    contract.methods
      .createWebshop("Test")
      .send({ from: accounts[0] })
      .then(async () => {
        const deployedWebshops = await contract.methods.getDeployedWebshops().call();
        console.log(deployedWebshops[deployedWebshops.length - 1]);
        console.log(deployedWebshops);
      });
  };

  try {
    return (
      <Container className="text-center">
        <Row className="mx-auto my-5">
          <Col>
            <h1>ETH Webshop</h1>
            <h2>Created by Bas Abbink</h2>
            <h3>{smartContractAddress}</h3>
          </Col>
        </Row>
        <Row className="mx-auto my-5">
          <Col>
            <Button onClick={createWebshop}>Create Webshop</Button>
          </Col>
        </Row>
      </Container>
    );
  } catch (err) {
    alert(err.message);
  }
}

export default App;
