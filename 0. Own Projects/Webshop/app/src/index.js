import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";

import App from "./App";
import Webshop from "./Shop/Webshop";

import "bootstrap/dist/css/bootstrap.min.css";

ReactDOM.render(
  <Router>
    <Route exact path="/" component={App}></Route>
    <Route exact path="/:webshopAddress" component={Webshop} />
  </Router>,
  document.getElementById("root")
);
