import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import webshopABI from "../Webshop-abi.json";

const Moralis = require("moralis");

function Webshop(props) {
  const { webshopAddress } = props.match.params;
  const [webshop, setWebshop] = useState({});
  const [products, setProducts] = useState({});

  const getShopName = async () => {
    const web3 = await Moralis.Web3.enable();
    const contract = new web3.eth.Contract(webshopABI, webshopAddress);
    setWebshop({ ...webshop, shopName: await contract.methods.getShopName().call() });
  };

  const getProducts = async () => {
    const web3 = await Moralis.Web3.enable();
    const contract = new web3.eth.Contract(webshopABI, webshopAddress);
    setProducts(await contract.methods.getShopName().call());
  };

  getShopName();
  getProducts();
  console.log(webshop);
  console.log(products);

  return (
    <Container className="text-center">
      <Row className="mx-auto my-5">
        <Col>
          <h1>{webshop.shopName}</h1>
        </Col>
      </Row>
    </Container>
  );
}

export default Webshop;
